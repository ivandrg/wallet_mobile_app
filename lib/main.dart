import 'package:flutter/material.dart';
import 'package:awesome_card/awesome_card.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SafeArea(
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  final List<Widget> _pages = [
    const InitialPage(),
    const Center(child:  Text('1')),
    // const Center(child:  Text('2')),
    const InitialPage(),
    const Center(child:  Text('3')),
  ];

  @override
  Widget build(BuildContext context) {
    double radius = 30.0;
    double heightbar = 80.0;
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
          gradient: const LinearGradient(
              colors: [Color(0xff4338CA), Color(0xff6D28D9)]),
        ),
        child: BottomAppBar(
          elevation: 0,
          color: Colors.transparent,
          child: SizedBox(
            height: heightbar,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconBottomBar(
                      label: "",
                      icon: Icons.home,
                      selected: _selectedIndex == 0,
                      onPressed: () {
                        setState(() {
                          _selectedIndex = 0;
                        });
                      }),
                  IconBottomBar(
                      label: "",
                      icon: Icons.search_outlined,
                      selected: _selectedIndex == 1,
                      onPressed: () {
                        setState(() {
                          _selectedIndex = 1;
                        });
                      }),
                  IconBottomBar(
                      label: "",
                      icon: Icons.add_to_photos_outlined,
                      selected: _selectedIndex == 2,
                      onPressed: () {
                        setState(() {
                          _selectedIndex = 2;
                        });
                      }),
                  IconBottomBar(
                      label: "",
                      icon: Icons.local_grocery_store_outlined,
                      selected: _selectedIndex == 3,
                      onPressed: () {
                        setState(() {
                          _selectedIndex = 3;
                        });
                      }),
                  // IconBottomBar(
                  //     text: "Calendar",
                  //     icon: Icons.date_range_outlined,
                  //     selected: false,
                  //     onPressed: () {})
                ],
              ),
            ),
          ),
        ),
      ),
      body: _pages[_selectedIndex],
    );
  }
}

class InitialPage extends StatefulWidget {
  const InitialPage({Key? key}) : super(key: key);

  @override
  State<InitialPage> createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  bool tapCard = false;
  @override
  Widget build(BuildContext context) {
    final _screensize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: _screensize.width,
              child: const HeaderWidget(),
            ),
            const SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  tapCard = !tapCard;
                });
              },
              child: CreditCardWidget(
                tapCard: tapCard,
              ),
            ),
            const SizedBox(
              height: 35,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButtonWidget(
                  colorBut: const Color(0XFFF9EBF5),
                  iconBut: Icons.shopping_cart_rounded,
                  label: 'Market',
                  onTap: () {
                    // print('1');
                  },
                ),
                IconButtonWidget(
                  colorBut: const Color(0XFFD3EAE6),
                  iconBut: Icons.credit_card,
                  label: 'Credit',
                  onTap: () {
                    // print('2');
                  },
                ),
                IconButtonWidget(
                  colorBut: const Color(0XFFE9E6F4),
                  iconBut: Icons.vpn_key,
                  label: 'Rent',
                  onTap: () {
                    // print('3');
                  },
                ),
                IconButtonWidget(
                  colorBut: const Color(0XFFC8E1F6),
                  iconBut: Icons.wallet_membership,
                  label: 'Payments',
                  onTap: () {
                    // print('4');
                  },
                ),
                IconButtonWidget(
                  colorBut: const Color(0XFFFBE3E1),
                  iconBut: Icons.phone,
                  label: 'Recharge',
                  onTap: () {
                    // print('5');
                  },
                )
              ],
            ),
            const SizedBox(
              height: 35,
            ),
            SizedBox(
              width: _screensize.width,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      Spacer(
                        flex: 1,
                      ),
                      Text(
                        'Transactions',
                        style: TextStyle(
                          color: Color(0XFF373841),
                          fontSize: 25,
                          letterSpacing: -1,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(
                        flex: 15,
                      ),
                    ],
                  ),
                  const ItemTransactionWidget(
                    title: 'Le Bernardine',
                    subtitle: 'Aug 27, 2021 at 5:19 PM',
                    amount: '120.50',
                    iconItem: Icons.dinner_dining,
                  ),
                  const ItemTransactionWidget(
                    title: 'Gym INC',
                    subtitle: 'Aug 24, 2021 at 9:10 AM',
                    amount: '220.00',
                    iconItem: Icons.g_mobiledata,
                  ),
                  const ItemTransactionWidget(
                    title: 'Wallmart',
                    subtitle: 'Aug 23, 2021 at 7:45 PM',
                    amount: '75.32',
                    iconItem: Icons.store,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ItemTransactionWidget extends StatelessWidget {
  const ItemTransactionWidget({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.amount,
    required this.iconItem,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String amount;
  final IconData iconItem;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: const Color(0XFFF6F5FA),
        ),
        child: Icon(
          iconItem,
          size: 35,
          color: const Color(0XFFB8B9C3),
        ),
      ),
      title: Text(
        title,
        style: const TextStyle(
          color: Color(0XFF373841),
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      subtitle: Text(
        subtitle,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14,
          color: Color(0XFFD8D9DD),
        ),
      ),
      trailing: Text(
        '-\$$amount',
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 21,
          color: Color(0XFF373841),
        ),
      ),
    );
  }
}

class IconButtonWidget extends StatelessWidget {
  const IconButtonWidget({
    Key? key,
    required Color colorBut,
    required IconData iconBut,
    required this.onTap,
    required this.label,
  })  : _colorBut = colorBut,
        _iconBut = iconBut,
        super(key: key);

  final Color _colorBut;
  final IconData _iconBut;
  final VoidCallback onTap;
  final String label;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 70),
      child: Column(
        children: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: const CircleBorder(),
              primary: _colorBut,
            ),
            child: Container(
              width: 50,
              height: 50,
              alignment: Alignment.center,
              child: Icon(
                _iconBut,
                color: const Color(0XFF373841),
              ),
            ),
            onPressed: onTap,
          ),
          const SizedBox(
            height: 15,
          ),
          Text(
            label,
            style: const TextStyle(
              color: Color(0XFF373841),
              fontWeight: FontWeight.w600,
              fontSize: 15,
              letterSpacing: -1,
            ),
          ),
        ],
      ),
    );
  }
}

class CreditCardWidget extends StatelessWidget {
  const CreditCardWidget({
    Key? key,
    required this.tapCard,
  }) : super(key: key);

  final bool tapCard;

  @override
  Widget build(BuildContext context) {
    return CreditCard(
      cardNumber: "****  ****  ****  7854",
      cardHolderName: "Card Holder",
      cvv: "456",
      bankName: "Axis Bank",
      cardType:
          CardType.masterCard, // Optional if you want to override Card Type
      showBackSide: tapCard,
      frontBackground: CardBackgrounds.black,
      backBackground: CardBackgrounds.white,
      showShadow: true,
      // textExpDate: 'Exp. Date',
      cardExpiry: "10/25",
      textName: 'hooa',
      textExpiry: 'MM/YY',
    );
  }
}

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Spacer(
          flex: 1,
        ),
        const CircleAvatar(
          radius: 30,
          child: Icon(Icons.person),
          foregroundImage: NetworkImage(
              'https://images.unsplash.com/photo-1519105548049-8e4ad05e2fa5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80'),
        ),
        const Spacer(
          flex: 1,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              'Welcome,',
              style: TextStyle(
                color: Color(0XFFCFCFCE),
                fontSize: 20,
                letterSpacing: -1,
              ),
            ),
            Text(
              'Cameron Howard',
              style: TextStyle(
                color: Color(0XFF373841),
                fontSize: 24,
                letterSpacing: -1,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        const Spacer(
          flex: 5,
        ),
        const Icon(Icons.notifications),
        const Spacer(
          flex: 1,
        ),
      ],
    );
  }
}

class BottomNavBarFb5 extends StatefulWidget {
  const BottomNavBarFb5({
    Key? key,
  }) : super(key: key);

  @override
  State<BottomNavBarFb5> createState() => _BottomNavBarFb5State();
}

class _BottomNavBarFb5State extends State<BottomNavBarFb5> {
  final primaryColor = const Color(0xff4338CA);
  final secondaryColor = const Color(0xff6D28D9);
  final accentColor = const Color(0xffffffff);
  final backgroundColor = const Color(0xffffffff);
  final errorColor = const Color(0xffEF4444);
  // int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    double radius = 30.0;
    double heightbar = 80.0;
    return Container();
  }
}

class IconBottomBar extends StatelessWidget {
  const IconBottomBar(
      {Key? key,
      required this.label,
      required this.icon,
      required this.selected,
      required this.onPressed})
      : super(key: key);
  final String label;
  final IconData icon;
  final bool selected;
  final Function() onPressed;

  final primaryColor = const Color(0xff4338CA);
  final accentColor = const Color(0xffffffff);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: onPressed,
          icon: Icon(
            icon,
            size: 32,
            color: selected ? accentColor : Colors.grey,
          ),
        ),
        Text(
          label,
          style: TextStyle(
            fontSize: 12,
            height: .1,
            color: selected ? accentColor : Colors.grey,
          ),
        )
      ],
    );
  }
}
